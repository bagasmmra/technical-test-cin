import React from "react";
import {
  Box,
  Flex,
  Image,
  Spinner,
} from "@chakra-ui/react";

export function GifItem({ url, embedURL }) {
  const [loaded, setLoaded] = React.useState(false);

  return (
    <Box
      rounded="lg"
      width={["320px", "280px", "240px"]}
      height={["320px", "280px", "240px"]}
      padding="2"
      bgColor="white.300"
      boxShadow="md"
      mx="auto"
    >
      <Image
        hidden={!loaded}
        rounded="lg"
        transition="transform 0.3s ease"
        height="100%"
        width="100%"
        src={url}
        onLoad={() => setLoaded(true)}
        objectFit="cover"
      />

      {!loaded && (
        <Flex
          bgColor="gray.50"
          w="100%"
          h="100%"
          align="center"
          justify="center"
        >
          <Spinner
            thickness="4px"
            speed={`${Math.random() / 10 + 0.8}s`}
            emptyColor="gray.200"
            color="green.300"
            size="md"
            width="40%"
            height="40%"
          />
        </Flex>
      )}
    </Box>
  );
}